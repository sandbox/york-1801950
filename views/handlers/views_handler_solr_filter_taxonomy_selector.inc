<?php
/**
 * Field handler to provide better exposed filter for indexed term fields
 * with fetching possible options from the vocabulary.
 */
class views_handler_solr_filter_taxonomy_selector extends SearchApiViewsHandlerFilter {

  function option_definition() {
    $options = parent::option_definition();

    $options['selected_vocabulary'] = array('default' => NULL, 'translatable' => TRUE);

    return $options;
  }

  /**
   * Extends the option form with a vocabulary selector
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $vocabulary = taxonomy_get_vocabularies();
    $options = array('0' => t('None'));
    foreach ($vocabulary as $vocab) {
      $options[$vocab->vid] = $vocab->name;
    }

    $form['selected_vocabulary'] = array(
      '#type' => 'select',
      '#title' => t('Vocabulary'),
      '#options' => $options,
      '#default_value' => $this->options['selected_vocabulary'],
    );
  }

  /**
   * Provides a dropdown select list with the term names
   */
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    $vocab = !empty($this->options['selected_vocabulary']) ? $this->options['selected_vocabulary'] : '';

    if (!empty($vocab)) {
      $tree = taxonomy_get_tree($vocab);
      $options = array();
      foreach ($tree as $term) {
        $options[$term->tid] = $term->name;
      }

      $form['value'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => isset($this->value) ? $this->value : NULL,
        '#title' => empty($form_state['exposed']) ? t('Value') : '',
      );
    }
  }
}
